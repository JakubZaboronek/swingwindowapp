package BookDatabase;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

class BookObjectOutputStream extends ObjectOutputStream {

    public BookObjectOutputStream(OutputStream out) throws IOException {
        super(out);
    }

    @Override
    protected void writeStreamHeader() throws IOException {
        if (BookFileService.EXISTS) {
            reset();
        } else {
            BookFileService.EXISTS = true;
            super.writeStreamHeader();
        }
    }


}
