package BookDatabase;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

public class BookListWindow extends AlertSupportJFrame {

    private JList<Book> bookJList=new JList<>();
    private DefaultListModel<Book> bookListModel=new DefaultListModel<>();
    private JButton addBookButton=new JButton("Dodaj książkę");
    private AddBookWindow addBookWindow=new AddBookWindow();
    private BookFileService bookFileService=new BookFileService();

    public BookListWindow() {
        setLayout(new BorderLayout());
        setSize(400,200);
        addComponents();
        addListeners();
        load();
        bookJList.setModel(bookListModel);
    }

    public void load(){
        List<Book> book=bookFileService.loadBooks();
        for (Book book1 : book) {
            bookListModel.addElement(book1);
        }
    }

    private void addListeners() {
        addBookButton.addActionListener(e ->addBookWindow.setVisible(true));
        addBookWindow.addButtonActionListener(new AddBookActionListener(bookListModel,addBookWindow));
    }

    private void addComponents() {
        add(bookJList,BorderLayout.CENTER);
        add(addBookButton,BorderLayout.PAGE_START);
    }
}
