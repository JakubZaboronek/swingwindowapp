package BookDatabase;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class BookFileService {
    public static final String DATABASE_LOCATION = "C:/Users/Jakub/BookTitle2.ser";
    public static boolean EXISTS = new File(BookFileService.DATABASE_LOCATION).exists();

    public void saveBook(Book book) {
        try(ObjectOutputStream out=new ObjectOutputStream(new FileOutputStream("C:/Users/Jakub/BookTitle2.ser",true))){
                out.writeObject(book);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public List<Book> loadBooks() {
        List<Book> books = new ArrayList<>();
        ObjectInputStream objectInput = null;
        try {
            objectInput = new ObjectInputStream(new FileInputStream("C:/Users/Jakub/BookTitle2.ser"));
            boolean end = false;
            while (!end) {
               Book book = (Book) objectInput.readObject();
                books.add(book);
            }
        } catch (IOException i) {
            i.printStackTrace();
        } catch (ClassNotFoundException c) {
            c.printStackTrace();
        }
        return books;
    }
}
