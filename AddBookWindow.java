package BookDatabase;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class AddBookWindow extends AlertSupportJFrame {
    private JTextField bookTitleField = new JTextField("Tytuł książki");
    private JButton addBookButton = new JButton("Dodaj książkę");
    private JButton selectAuthorButton = new JButton("Wybierz autora");
    private AuthorListWindow authorListWindow=new AuthorListWindow();

    public AddBookWindow() {
        setLayout(new FlowLayout());
        setSize(300, 100);
        addComponents();
        addListeners();
    }

    private void addComponents() {
        add(addBookButton);
        add(bookTitleField);
        add(selectAuthorButton);
    }

    private void addListeners() {
        selectAuthorButton.addActionListener(e -> authorListWindow.setVisible(true));
    }

    public void addButtonActionListener(ActionListener listener) {
        addBookButton.addActionListener(listener);
    }

    public Book createBook() {
        if(bookTitleField.getText().length()<1){
            showAlert("Nie napisales tytulu");
            return null;
        }
        else {
            return CreateBook.createBook(bookTitleField.getText(),authorListWindow.getSelectedAuthor());
        }
    }
}
