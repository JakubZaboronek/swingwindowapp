package BookDatabase;

import javax.swing.*;
import java.awt.*;

public class AuthorListWindow extends AlertSupportJFrame {

    public JList<Author> authorList=new JList<>();
    private DefaultListModel<Author> authorListModel=new DefaultListModel<>();
    private JButton addAuthorButton=new JButton("Dodaj autora");
    private AddAuthorWindow addAuthorWindow=new AddAuthorWindow();

    public Author getSelectedAuthor(){
        return authorList.getSelectedValue();
    }

    public AuthorListWindow(){
        setLayout(new BorderLayout());
        setSize(400,200);
        addComponents();
        addListeners();
        authorList.setModel(authorListModel);

    }

    private void addComponents() {
        add(authorList,BorderLayout.CENTER);
        add(addAuthorButton,BorderLayout.PAGE_START);
    }
    private void addListeners() {
        addAuthorButton.addActionListener(e -> addAuthorWindow.setVisible(true));
        addAuthorWindow.addAddAuthorActionListener(e -> authorListModel.addElement(addAuthorWindow.createAuthor()));
    }
}
