package BookDatabase;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddBookActionListener implements ActionListener {
    private DefaultListModel<Book> listModel;
    private AddBookWindow addBookWindow;
    private BookFileService bookFileService=new BookFileService();

    public AddBookActionListener(DefaultListModel<Book> listModel, AddBookWindow addBookWindow) {
        this.listModel = listModel;
        this.addBookWindow = addBookWindow;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
            listModel.addElement(addBookWindow.createBook());
            bookFileService.saveBook(addBookWindow.createBook());
    }
}
