package BookDatabase;

import java.util.Optional;

public class CreateBook {

    public static Book createBook(String title, Author author){
        Book book=new Book();
        book.setTitle(title);
        book.setAuthor(author);
        return book;
    }
}
//    public static Optional<Book> createBook(String title, Author author){
//        Book book=new Book();
//        book.setTitle(title);
//        book.setAuthor(author);
//        return Optional.ofNullable(book);
//    }
