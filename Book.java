package BookDatabase;

import java.io.Serializable;

public class Book implements Serializable {
    private Author author;
    private String title;

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Tytuł książki to: "+getTitle()+"Autor to: "+getAuthor();
    }
}
