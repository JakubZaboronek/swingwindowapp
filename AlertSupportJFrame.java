package BookDatabase;

import javax.swing.*;

public  abstract class AlertSupportJFrame extends JFrame {

    public void showAlert(String message){
        JOptionPane.showMessageDialog(this,message);
    }
}
