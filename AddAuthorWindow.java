package BookDatabase;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class AddAuthorWindow extends AlertSupportJFrame {

    private JTextField authorLastName=new JTextField("Imię autora");
    private JTextField authorFirstName=new JTextField("nazwisko autora");
    private JButton addAuthorButton=new JButton("Dodaj autora");

    public AddAuthorWindow(){
        setLayout(new FlowLayout());
        setSize(300,100);
        addComponents();
    }

    private void addComponents() {
        add(addAuthorButton);
        add(authorFirstName);
        add(authorLastName);
    }

    public Author createAuthor(){
        Author author=new Author();
        if(authorFirstName.getText().length()<1){
            showAlert("Nie napisales Imienia Autora");
            return null;
        }
        else if(authorLastName.getText().length()<1){
            showAlert("Nie napisales nazwiska Autora");
            return null;
        }
        else if(authorFirstName.getText().length()<1||authorLastName.getText().length()<1){
            showAlert("Nie napisales imienia i nazwiska Autora");
            return null;
        }
        else{
            author.setFirstName(authorFirstName.getText());
            author.setLastName(authorLastName.getText());
            return author;
        }
    }
    public void addAddAuthorActionListener(ActionListener listener) {
        addAuthorButton.addActionListener(listener);
    }

}
